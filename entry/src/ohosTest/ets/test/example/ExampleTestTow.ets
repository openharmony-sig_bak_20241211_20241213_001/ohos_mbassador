/*
 * Copyright (C) 2022-2024 Huawei Device Co., Ltd.
 * Licensed under the MIT License, (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://opensource.org/licenses/MIT
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import { Annotations, References } from '@ohos/mbassador'
import { ExampleFilter } from './ExampleFilter'
import { CallBackListener } from './CallBackListener'
import { TestInfo } from './TestInfo'


@Annotations.Listener("ExampleTestTow",References.Weak)
export class ExampleTestTow {
  call: CallBackListener;

  constructor(call: CallBackListener) {
    this.call = call;
  }

  @Annotations.handle()
  firstMessageTwo(str: TestInfo, listener: ExampleTestTow) {
    listener.call.callData(("firstMessageTwo"+str.strName), 0);
  }

  @Annotations.handle({ priority: 100000, enabled: false })
  secondMessageTwo(str: TestInfo, listener: ExampleTestTow) {
    listener.call.callData(("secondMessageTwo"+str.strName), 1);
  }

  @Annotations.handle({ filters: [new ExampleFilter()], enabled: true })
  thirdMessageTwo(str: TestInfo, listener: ExampleTestTow) {
    listener.call.callData(("thirdMessageTwo"+str.strName), 2);
  }
}
